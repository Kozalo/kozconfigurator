KozConfigurator v0.4.0
======================

Для русскоговорящих
-------------------

```
Леонид Козарин <kozalo@nekochan.ru>
Лицензия: MIT
(c) Kozalo.Ru, 2017-2018

Только для Debian!
Протестировано на Debian 9 (stretch)
```

Этот скрипт помогает провести первоначальную и максимально полную настройку свежеустановленной Debian-системы в автоматическом режиме.  
Например, он включает `contrib` и `non-free` репозитории и устанавливает поддержку HTTPS-протокола для APTа. Также он включает поддержку 32-битных пакетов для 64-битных систем.  
Правда стоит отметить, что скрипт подразумевает, что при установке в качестве основного зеркала Вы выбрали сервер Debian, выделенный под Вашу страну (в моём случае это <http://ftp.ru.debian.org/debian>).
Если это не так, то для загрузки всех пакетов он будет использовать главный репозиторий (<http://ftp.debian.org/debian>), что может несколько замедлить процесс.
Впоследствии можно восстановить исходный список репозиториев из бэкап-файла (`/etc/apt/sources.list.bak`).  

Программы, устанавливаемые скриптом, делятся на различные группы. Набор обязательного (на мой скромный взгляд) софта состоит из:  

* sudo, curl, vim, git  
* zsh, oh-my-zsh (клёвая командная оболочка с плагинами)  
* Midnight Commander, ranger (консольные файловые менеджеры)  
* dos2unix (CRLF => LF)  
* compton (композитный менеджер)  
* LibreOffice  
* GIMP  
* VLC, mplayer, moc (музыкальный плеер)  
* Chromium  
* shutter, scrot (скриншотеры)  
* wine (с winetricks, winecfg, и набором некоторых DLLок)  
* Telegram, VK Messenger  
* Transmission (торрент-клиент)  
* neofetch (скрипт для вывода информации о системе)  
* caja-open-terminal или nautilus-open-terminal  
* Kazam (программа для записи видео с экрана)  
* remmina (клиент для подключения к удалённому рабочему столу)  
* Java Runtime Environment  
* cmatrix (скринсейвер)  
* links2 (консольный браузер)  
* htop, iotop, tcptrack  
* zip, unzip, p7zip  
* guake (выпадающий терминал)  

Остальные наборы опциональны:  

* Набор геймера [-g]:  
    * sgfxi (скрипт, который помогает устанавливать проприетарные драйвера на видеокарту)  
    * Steam  
    * PlayOnLinux  
    * mesa-utils  

* Набор облачных хранилищ и синхронизации файлов [-c]:  
    * Яндекс.Диск  
    * Dropbox  

* Набор для построения локальной сети [-n]:  
    * Samba  
    * smbclient, cifs-utils  
    * ksharman (самописный скрипт, предназначенный для облегчения открытия и закрытия доступа к папкам)  
    * kremount (самописный скрипт для облегчения монтирования удалённых папок в файловую систему)  

* Набор анонима [-t]:  
    * Tor Browser  

* Набор для виртуализации [-v]:  
    * VirtualBox, dkms  

* Базовый набор разработчика (устанавливается, если выбран хотя бы один из нижерасположенных наборов):  
    * Git, Mercurial  
    * TortoiseHg, gitg  
    * Sublime Text 3  
    * FiraCode (шрифт с лигатурами)  

* Набор веб-разработчика [-w]:  
    * Apache2, PHP (+ debpear), MySQL, PhpMyAdmin  
    * SQLite 3  
    * Composer  
    * NodeJS, npm, bower  
    * PhpStorm, DataGrip  

* Набор Python-разработчика [-p]:  
    * python, python3 (если по какой-то причине ещё не установлены)  
    * pip  
    * SQLite 3  
    * PyCharm  

* Набор Android-разработчика [-a]:  
    * Android Studio  

* Набор Java-разработчика [-j]:  
    * IntelliJ IDEA  

* Набор C#-разработчика [-s]:  
    * MonoDevelop  

* Набор низкоуровневого разработчика [-e]:  
    * gcc-avr, binutils-avr, avr-libc  
    * avrdude  
    * minicom  

* Набор оконного менеджера i3 [-i]:  
    * i3 (тайловый/фреймовый оконный менеджер)  
    * i3blocks (статус-бар)  
    * feh (просмотрщик изображений с возможностью устанавливать фоновое изображение рабочего стола)  
    * lxappearance (утилита для изменения тем GTK)  
    * pavucontrol, pactl (управление звуком)  
    * dconf-editor  
    * xserver-xorg, xinit, xclip (X Window System)  
    * lightdm (экран входа в систему)  
    * network-manager-gnome  
    * gxkb (переключатель раскладки клавиатуры)  

Наборы подключаются либо с помощью соответствующих опций командной строки, либо в интерактивном режиме при запуске команды.
Например для установки абсолютно всех программ, нужно ввести следующую команду:  

```
su -c './KozConfigurator.sh -gcntvwpajsei'      # Не забудьте запустить скрипт с правами администратора!
```

Скрипт старается работать как можно более автоматически, но всё же при установке некоторых программ (например, Wine, системы контроля версий или MySQL) будут запрашиваться дополнительные данные.  

Для устанавливаемых программ создаются ссылки как для запуска через меню приложений, так и через терминал.  
Большие программы без собственного установщика (например, среды разработки от JetBrains) устанавливаются в директорию `/opt/KozConfigurator`, но для них также создаются необходимые ссылки.  

Любителей дополнительной подстройки системы под себя могут заинтересовать [мои конфиги](https://github.com/kozalosev/dotfiles) в качестве отправной точки.  


In English
----------

```
Leonid Kozarin <kozalo@nekochan.ru>
License: MIT
(c) Copyright Kozalo.Ru, 2017-2018

Debian only!
Tested on Debian 9 (stretch)
```

This script is intended to help you to perform the initial configuration of your system after you've just installed it.  
Generally, the script represents my perception of a minimal configured and really ready-to-use Debian system.  

First of all, it enables `contrib` and `non-free` repositories for the standard Debian sources. If you're using a 64-bit system, it enables support for i386 packages too.  
HTTPS transport for APT also will be installed.  

Then the script installs some basic software (in my humble opinion):  

* sudo, curl, vim, git  
* zsh, oh-my-zsh (awesome shell with plugins)  
* Midnight Commander, ranger (console file managers)  
* dos2unix (CRLF => LF)  
* compton (composite manager)  
* LibreOffice  
* GIMP  
* VLC, mplayer, moc (music player)  
* Chromium  
* shutter, scrot (screenshotters)  
* wine (with winetricks, winecfg, and a bunch of DLL modules)  
* Telegram, VK Messenger  
* Transmission (torrent client)  
* neofetch (prints system information)  
* caja-open-terminal or nautilus-open-terminal  
* Kazam (screen video recorder)  
* remmina (remote desktop client)  
* Java Runtime Environment  
* cmatrix (screensaver)  
* links2 (console browser)  
* htop, iotop, tcptrack  
* zip, unzip, p7zip  
* guake (drop-down terminal)  

Also, it installs some drivers for your hardware. The rest of software are optional. You're able to choose which programs to install by your own. Just pass corresponding parameters when you runs the script.  
For instance, this example explains how to install ALL programs:  

```
su -c './KozConfigurator.sh -gcntvwpajsei'       # Don't forget to run the script on behalf of the root!
```

Let's consider all possible options:  

* Gamer's set [-g]:  
    * sgfxi (a helper script to let you install proprietary video card drivers with ease)  
    * Steam  
    * PlayOnLinux  
    * mesa-utils  

* Cloud services and synchronization set [-c]:  
    * Yandex.Disk  
    * Dropbox  

* Set for building a local network [-n]:  
    * Samba  
    * smbclient, cifs-utils  
    * ksharman (self-made script to make easier for you to share folders)  
    * kremount (self-made script to help you mount remote folders in the local network)  

* Anonymous' set [-t]:  
    * Tor Browser  

* Virtualization set [-v]:  
    * VirtualBox, dkms  

* Basic set for developers (it will be installed if you choose at least one of options below):  
    * Git, Mercurial  
    * TortoiseHg, gitg  
    * Sublime Text 3  
    * FiraCode (font with ligatures)  

* Web developer's set [-w]:  
    * Apache2, PHP (+ debpear), MySQL, PhpMyAdmin  
    * SQLite 3  
    * Composer  
    * NodeJS, npm, bower  
    * PhpStorm, DataGrip  

* Python developer's set [-p]:  
    * python, python3 (if not installed yet for any reason)  
    * pip  
    * SQLite 3  
    * PyCharm  

* Android developer's set [-a]:  
    * Android Studio  

* Java developer's set [-j]:  
    * IntelliJ IDEA  

* C# developer's set [-s]:  
    * MonoDevelop  

* Embedded developer's set [-e]:  
    * gcc-avr, binutils-avr, avr-libc  
    * avrdude  
    * minicom  

* i3 set [-i]:  
    * i3 (tiling window manager)  
    * i3blocks (status bar)  
    * feh (image viewer with ability to set a background)  
    * lxappearance (utility to change GTK themes)  
    * pavucontrol, pactl (audio management)  
    * dconf-editor  
    * xserver-xorg, xinit, xclip (X Window System)  
    * lightdm (display manager)  
    * network-manager-gnome  
    * gxkb (keyboard layout switcher)  

Note, despite the fact that most of the software are installed automatically, Wine will require your attention to click some buttons and manually download one file from the Internet. VCSes and MySQL will also require your attention.  
Another very important thing is the script makes an assumption that you've chosen region based repositories like <http://ftp.ru.debian.org/debian>.
If this assumption is not true, it downloads software from the main repository (<http://ftp.debian.org/debian>).
After the script finishes its job, execute you can manually restore original repositories in the `/etc/apt/sources.list` using the `/etc/apt/sources.list.bak` file.

Also, the script creates links for the programs to let you run them using the desktop environment.
All big programs (such as JetBrains' software, for example) will be installed in the `/opt/KozConfigurator/` directory. But symlinks for them will be created in the `/usr/local/bin/` directory.  

If you wish to tweak the system even more to make it completely consistent with your need, you may be interested in [my configs](https://github.com/kozalosev/dotfiles) to use them as a starting point.